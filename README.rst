README
=================================

This is the README file of a LaTeX beamer theme that should 
follow the graphical identity guidelines of the University of Milan.


.. warning::
   This is not an official theme of the University of Milan.
   
.. note::
   At the moment, the implementation is "quick and dirty", for example the logo and the background
   should be also included in the ``img`` folder of the slides, and so on...
   Also, the fonts are not 100% in agreement with the guidelines of UNIMI.

|
What is this repository for?
===================================

* This repository contains a LaTeX beamer package for creating slides
  with the logo of the University of Milan.
* Version 0.1

|  
How do I get set up?
=============================
To install this beamer theme on a Unix like machine, you should put all the directories, color,
font, inner, outer and theme into a directory like:
``~/texmf/tex/latex/beamer``, where ``~`` is your home directory.

For other platforms (i.e. MikTeX on MS Windows), the following link
contains useful hints for the configuration:
`https://tex.stackexchange.com/questions/123312/correct-path-on-windows-for-custom-beamer-style-files
<https://tex.stackexchange.com/questions/123312/correct-path-on-windows-for-custom-beamer-style-files>`_.

|
Who do I talk to?
=========================
* This theme was derived from the original work of Till Tantau, with
  minor modifications by Alessandro Comunian.
* It is now maintained by the Geophysics for the Environment and Cultural Heritage (GECH) Lab. of the University of
  Milan (`https://sites.unimi.it/gechlab
  <https://sites.unimi.it/gechlab>`_)
